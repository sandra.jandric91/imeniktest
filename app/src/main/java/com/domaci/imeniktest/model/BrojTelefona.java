package com.domaci.imeniktest.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = BrojTelefona.TABLE_NAME_BROJ)
public class BrojTelefona {

    public static final String TABLE_NAME_BROJ = "brojevi";

    public static final String FIELD_BROJ_ID = "id";
    public static final String FIELD_BROJ_BROJ = "broj";
    public static final String FIELD_BROJ_TIP = "tip";

    @DatabaseField(columnName = FIELD_BROJ_ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = FIELD_BROJ_BROJ)
    private String broj;
    @DatabaseField(columnName = FIELD_BROJ_TIP)
    private String tip;
    @DatabaseField(columnName = Kontakt.TABLE_NAME_KONTAKT, foreign = true, foreignAutoRefresh = true)
    private Kontakt kontakt;

    public BrojTelefona() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBroj() {
        return broj;
    }

    public void setBroj(String broj) {
        this.broj = broj;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Kontakt getKontakt() {
        return kontakt;
    }

    public void setKontakt(Kontakt kontakt) {
        this.kontakt = kontakt;
    }
}
