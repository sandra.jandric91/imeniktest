package com.domaci.imeniktest.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.domaci.imeniktest.AboutDijalog;
import com.domaci.imeniktest.App;
import com.domaci.imeniktest.DatabaseHelper;
import com.domaci.imeniktest.R;
import com.domaci.imeniktest.model.BrojTelefona;
import com.domaci.imeniktest.model.Kontakt;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.ForeignCollection;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView imageView;
    private EditText etImeDetalji;
    private EditText etPrezimeDetalji;
    private EditText etAdresaDetalji;
    private Spinner spinner;
    private EditText etBroj;
    private EditText etTip;

    private Button btnDodajBroj;
    private Button btnObrisiKontakt;
    private Button btnIzmeniKontakt;

    private List<BrojTelefona> telefoniLista;
    private ArrayAdapter adapter;

    private Kontakt kontakt;
    private BrojTelefona brojTelefona = null;
    private DatabaseHelper databaseHelper;

    private AlertDialog aboutDijalog;
    private SharedPreferences sharedPreferences;
    public static String TOAST_SETTINGS = "toast_settings_cb";
    public static String NOTIF_SETTINGS = "notif_settings_cb";
    public static final int NOTIF_ID = 1;

    private static final int SELECT_PICTURE = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setupToolbar();
        dodajTip();
        showList();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void dodajTip() {
        brojTelefona = new BrojTelefona();
        brojTelefona.setTip("Fiksni telefon");

        try {
            getDatabaseHelper().getBrojTelefonaDao().create(brojTelefona);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        brojTelefona = new BrojTelefona();
        brojTelefona.setTip("Mobilni telefon");

        try {
            getDatabaseHelper().getBrojTelefonaDao().create(brojTelefona);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        brojTelefona = new BrojTelefona();
        brojTelefona.setTip("Poslovni telefon");

        try {
            getDatabaseHelper().getBrojTelefonaDao().create(brojTelefona);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showList() {

        imageView = findViewById(R.id.imageView);
        etImeDetalji = findViewById(R.id.etImeDetalji);
        etPrezimeDetalji = findViewById(R.id.etPrezimeDetalji);
        etAdresaDetalji = findViewById(R.id.etAdresaDetalji);
        etBroj = findViewById(R.id.etBroj);
        etTip = findViewById(R.id.etTip);
        spinner = findViewById(R.id.spinnerKategorija);
        setupAdapter();

        btnDodajBroj = findViewById(R.id.btnDodajBroj);
        btnObrisiKontakt = findViewById(R.id.btnObrisiKontakt);
        btnIzmeniKontakt = findViewById(R.id.btnIzmeniKontakt);

        btnDodajBroj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tip = String.valueOf(spinner.getSelectedItem());
                etTip.setText(tip);
                etBroj.setText(etBroj.getText().toString());
                if (brojTelefona != null) {
                    brojTelefona.setTip(etTip.getText().toString());
                    brojTelefona.setBroj(etBroj.getText().toString());
                    try {
                        getDatabaseHelper().getBrojTelefonaDao().update(brojTelefona);
                        Log.v("BBBB", "broj unet u bazu");
                        showToast("Broj dodat");
                        showNotif("Broj dodat", getApplicationContext());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        int id = getIntent().getExtras().getInt("kontaktID");

        try {
            kontakt = getDatabaseHelper().getKontaktDao().queryForId(id);
            brojTelefona = getDatabaseHelper().getBrojTelefonaDao().queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    //    imageView.setImageBitmap(kontakt.getSlika());
        etImeDetalji.setText(kontakt.getIme());
        etPrezimeDetalji.setText(kontakt.getPrezime());
        etAdresaDetalji.setText(kontakt.getAdresa());
        etTip.setText(brojTelefona.getTip());
        etBroj.setText(brojTelefona.getBroj());

        btnObrisiKontakt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (kontakt != null) {
                    try {
                        getDatabaseHelper().getKontaktDao().delete(kontakt);
                        showToast("Kontakt obrisan");
                        showNotif("Kontakt obrisan", getApplicationContext());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                onBackPressed();
            }
        });

        btnIzmeniKontakt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (kontakt != null) {
                    kontakt.setIme(etImeDetalji.getText().toString());
                    kontakt.setPrezime(etPrezimeDetalji.getText().toString());
                    kontakt.setAdresa(etAdresaDetalji.getText().toString());

                    try {
                        getDatabaseHelper().getKontaktDao().update(kontakt);
                        showToast("Kontakt izmenjen");
                        showNotif("Kontakt izmenjen", getApplicationContext());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                onBackPressed();
            }
        });
    }

    private void setupAdapter() {
        try {
            telefoniLista = getDatabaseHelper().getBrojTelefonaDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (telefoniLista == null) {
            telefoniLista = new ArrayList<>(telefoniLista);
        }

        spinner = findViewById(R.id.spinnerKategorija);
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.tip_telefona));
        spinner.setAdapter(adapter);
        String [] data = getResources().getStringArray(R.array.tip_telefona);
        for (int i = 0; i < data.length; i++) {
            if (data[i].equalsIgnoreCase(brojTelefona.getTip())) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showNotif(String message, Context context) {
        boolean notif = sharedPreferences.getBoolean(NOTIF_SETTINGS, false);
        if (notif) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, App.CHANNEL_ID);
            builder.setSmallIcon(R.drawable.ic_action_settings);
            builder.setContentTitle(message);
            builder.setContentText(message);
            notificationManager.notify(NOTIF_ID, builder.build());
        }
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.slika:
                showSlika();
                break;
            case R.id.settings:
                showSettings();
                break;
            case R.id.about:
                showDijalog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSlika() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Odaberi sliku"), SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();

                ImageView imageView = findViewById(R.id.imageView);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    imageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }

    private void showSettings() {
        Intent  intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
