package com.domaci.imeniktest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.domaci.imeniktest.model.BrojTelefona;
import com.domaci.imeniktest.model.Kontakt;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "contacts.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private Dao<Kontakt, Integer> kontaktDao = null;
    private Dao<BrojTelefona, Integer> brojTelefonaDao = null;

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Kontakt.class);
            TableUtils.createTable(connectionSource, BrojTelefona.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Kontakt.class, true);
            TableUtils.dropTable(connectionSource, BrojTelefona.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<Kontakt, Integer> getKontaktDao() throws SQLException {
        if (kontaktDao == null) {
            kontaktDao = getDao(Kontakt.class);
        }
        return kontaktDao;
    }

    public Dao<BrojTelefona, Integer> getBrojTelefonaDao() throws SQLException{
        if (brojTelefonaDao == null) {
            brojTelefonaDao = getDao(BrojTelefona.class);
        }
        return brojTelefonaDao;
    }

    @Override
    public void close() {
        kontaktDao = null;
        brojTelefonaDao = null;
        super.close();
    }
}
